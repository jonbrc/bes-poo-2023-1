package br.ucsal.bes20321.poo.lista01;
import java.util.Scanner;

public class questao_01 {

	public static void main(String[] args) {
		exibirConceito(gerarConceito(receberNota()));

	}
	
	public static String gerarConceito(int nota) {
		String conceito ="";
		if(nota>=0&&nota<=49) {
			conceito="insuficiente";
		}else if(nota>=50 && nota<=64) {
			conceito="regular";
		}else if(nota>=65 && nota<=84) {
			conceito="bom";
		}else {
			conceito="ótimo";
		}	
		return conceito;
	}
	public static int receberNota() {
		System.out.println("Insira a nota");
		Scanner sc= new Scanner(System.in);
		int nota=sc.nextInt();	
		return nota;
	}
	public static void exibirConceito(String conceito) {
		System.out.println("Seu conceito foi " +conceito);
	}

}
