package br.ucsal.bes20321.poo.lista01;
import java.util.Iterator;
import java.util.Scanner;

public class questao_04 {

	public static void main(String[] args) {
		String [] nome = receberNome();
		double [] altura = receberAltura();
		double [] peso = receberPeso();
		
		definirAltura(nome, altura);
		definirPeso(nome, peso);
	}
	public static String[] receberNome() {
		Scanner sc = new Scanner(System.in);

		System.out.println("indique os 10 nomes");
		String [] nome = new String [10];
		for (int i = 0; i < nome.length; i++) {
			nome[i]=sc.next();
		}
	 	return nome;
		
	}
	public static double[] receberPeso() {
		Scanner sc = new Scanner(System.in);

		System.out.println("indique os 10 pesos");
		double [] peso = new double [10];
		for (int i = 0; i < peso.length; i++) {
			peso[i]=sc.nextDouble();
		}
		return peso;
		
	}
	public static double[] receberAltura() {
		Scanner sc = new Scanner(System.in);

		System.out.println("indique as 10 alturas");
		double [] altura = new double [10];
		for (int i = 0; i < altura.length; i++) {
			altura[i]=sc.nextDouble();
		}
	 	return altura;
	}
	
	public static void definirAltura(String[] nome,double[] altura) {
		double alto = 0;
		int indice = 0;
		for (int i = 0; i < altura.length; i++) {
			if(altura[i] > alto) {
				alto=altura[i];
				indice = i;
			}
		}
		System.out.println("o mais alto é "+nome[indice]+" com "+alto+" de altura");
	}
	public static void definirPeso(String[] nome,double[] peso) {
		double pesado = 0;
		int indice = 0;
		for (int i = 0; i < peso.length; i++) {
			if(peso[i] > pesado) {
				pesado=peso[i];
				indice = i;
			}
		}
		System.out.println("o mais pesado é "+nome[indice]+" pesando "+pesado+" Kg");
	}
}

