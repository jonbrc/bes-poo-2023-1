package br.ucsal.bes20321.poo.lista01;
import java.util.Scanner;

public class questao_05 {

	public static void main(String[] args) {

		int termos = numeroTermos();
		double razao = razao();
		double primeiro = primeiroTermo();
		double [] progres = termos(termos, razao, primeiro);

		print.imprimir(progres);
		print.imprimir(soma(progres));


	}

	public static int numeroTermos() {
		Scanner sc = new Scanner (System.in);
		System.out.println("indique a quantidade de termos");
		int numero = sc.nextInt();
		return numero;
	}
	public static double razao() {
		Scanner sc = new Scanner (System.in);
		System.out.println("indique a razao");
		double razao = sc.nextDouble();
		return razao;
	}
	public static double primeiroTermo() {
		Scanner sc = new Scanner (System.in);
		System.out.println("indique o primeiro termo");
		double primeiro = sc.nextDouble();
		return primeiro;
	}

	public static double[] termos(int termos,double razao,double primeiro) {
		double [] progres = new double [termos];

		for (int i = 0; i < termos; i++) {
			progres[i] = primeiro;
			primeiro*=razao;
		}
		return progres;
	}
	public static double soma(double[] termos) {	
		double soma =0;
		for (int i = 0; i < termos.length; i++) {
			soma+=termos[i];
		}
		return soma;
	}
}
