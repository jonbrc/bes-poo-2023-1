package br.ucsal.bes20231.poo.lista03;
import java.util.Iterator;
import java.util.Scanner;

public class ConstruirLiga {

	public static void main(String[] args) {
	
		//criando jogadores
		Jogador joao = new Jogador("joao",11,"titular","zagueiro");
		Jogador gabriel = new Jogador("joao",14,"titular","goleiro");
		Jogador eduardo = new Jogador("joao",9,"titular","atacante");
		Jogador tauan = new Jogador("joao",10,"titular","centro-avante");
		Jogador yuri = new Jogador("joao",13,"titular","lateral esquerda");
		Jogador lian = new Jogador("joao",7,"titular","zagueiro");
		
		Jogador[] jogadoresVitoria = new Jogador[3];
		jogadoresVitoria[0] = joao;
		jogadoresVitoria[1] = gabriel;
		jogadoresVitoria[2] = eduardo;
		
		Jogador[] jogadoresBahia = new Jogador[3];
		jogadoresVitoria[0] = tauan;
		jogadoresVitoria[1] = yuri;
		jogadoresVitoria[2] = lian;
		
		//criando estadios
		Estadio barradao = new Estadio("barradao","06/11/1986","R. Art�mio Castro Valente, 1 - Canabrava, Salvador - BA, 41260-300", 30618);
		Estadio fonteNova = new Estadio("Fonte nova","07/04/2013","Ladeira da Fonte das Pedras, s/n - Nazar�, Salvador - BA, 40050-565", 48902);

		//criando time
		Time vitoria = new Time("vitoria","13/05/1899",barradao,jogadoresVitoria);
		Time bahia = new Time("vitoria","13/05/1899",fonteNova,jogadoresBahia);
			
		Time[] clubesBahianos = new Time[2];
		
		//criando liga
		Liga copaNordeste = new Liga("copa do nordeste", "regional",clubesBahianos,"A");
	}
}
