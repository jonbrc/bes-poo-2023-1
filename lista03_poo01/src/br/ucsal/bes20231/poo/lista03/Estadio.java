package br.ucsal.bes20231.poo.lista03;

public class Estadio {
	public Estadio(String nome, String dataInauguracao, String endereco, int capacidade) {
		super();
		this.nome = nome;
		this.dataInauguracao = dataInauguracao;
		this.endereco = endereco;
		this.capacidade = capacidade;
	}
	
	String nome;
	String dataInauguracao;
	String endereco;
	int capacidade;
}
