package br.ucsal.bes20231.poo.lista03;

public class Jogador {

	
	public Jogador(String nome, int numeroCamisa, String situacao, String posicao) {
		super();
		this.nome = nome;
		this.numeroCamisa = numeroCamisa;
		this.situacao = situacao;
		this.posicao = posicao;
	}
	
	String nome;
	int numeroCamisa;
	//titular ou reserva
	String situacao;
	String posicao;
}
