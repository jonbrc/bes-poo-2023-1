package br.ucsal.bes20231.poo.lista03;

public class Liga {
	public Liga(String nome, String tipo, Time[] timesParticipantes, String nivel) {
		super();
		this.nome = nome;
		this.tipo = tipo;
		this.timesParticipantes = timesParticipantes;
		this.nivel = nivel;
	}
	
	String nome;
	//regional ou nacional
	String tipo;
	Time[] timesParticipantes;
	//serie A,B ou C
	String nivel;
}
