package br.ucsal.bes20231.poo.lista03;

public class Time {
	public Time(String nome, String dataFundacao, Estadio nomeEstadio, Jogador[] nomeJogadores) {
		super();
		this.nome = nome;
		this.dataFundacao = dataFundacao;
		this.estadio = nomeEstadio;
		this.jogadores = nomeJogadores;
	}
	
	String nome;
	String dataFundacao;
	Estadio estadio;
	Jogador[] jogadores;
}
