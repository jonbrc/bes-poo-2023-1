package br.ucsal.bes20231.poo.listao;
import java.math.BigDecimal;
import java.util.Scanner;

public class questao_04 {


	public static void main(String[] args) {
		System.out.println(getArea());

	}

	public static BigDecimal getArea() {
		BigDecimal raio= getRaio();
		BigDecimal pi = new BigDecimal(3.14159);
		BigDecimal area = pi.multiply(raio).multiply(raio);
		
		return area;

	}
	public static BigDecimal getRaio() {
		Scanner sc = new Scanner (System.in);
		System.out.println("indique o Raio");
		BigDecimal raio = sc.nextBigDecimal();
		
		return raio;
	}




}
