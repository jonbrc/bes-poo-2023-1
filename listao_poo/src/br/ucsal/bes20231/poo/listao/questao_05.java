package br.ucsal.bes20231.poo.listao;
import java.util.Scanner;

public class questao_05 {

	public static void main(String[] args) {
		System.out.println("o valor da �rea � " + getArea());
	}
	
	public static double getBase() {
		Scanner sc = new Scanner (System.in);
		System.out.println("indique o valor da base");
		double base = sc.nextDouble();
		
		return base;
	}
	public static double getAltura() {
		Scanner sc = new Scanner (System.in);
		System.out.println("indique o valor da altura");
		double altura = sc.nextDouble();
		
		return altura;
	}
	public static double getArea() {
		final int DIVISOR = 2;
		double base = getBase();
		double altura = getAltura();
		double area = (base*altura)/DIVISOR;
		return area;
	}

}
