package br.ucsal.bes20231.poo.listao;
import java.util.Scanner;

public class questao_08 {

	public static void main(String[] args) {
		double horas = getHoras();
		double valorHoras = getValorHoras();
		double desc = getPerDesc();
		double salBruto = salBruto(horas,valorHoras);
		double totalDesc = totalDesc(desc, salBruto);
		
		System.out.println("Horas trabalhadas: ");
		printUtil.imprimir(horas);
		
		System.out.println("Valor Horas: ");
		printUtil.imprimir(valorHoras);
		
		System.out.println("Percentual de desconto: ");
		printUtil.imprimir(desc);
		
		System.out.println("Sal�rio Bruto: ");
		printUtil.imprimir(salBruto);
		
		System.out.println("Total de desconto: ");
		printUtil.imprimir(totalDesc);
		
		System.out.println("Sal�rio liquido: ");
		printUtil.imprimir(salLiquido(salBruto,totalDesc));
						
	}
	public static double getHoras() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Indique a quantidade de horas trabalhadas: ");
		double ht = sc.nextDouble();
		//horas trabalhadas
		return ht;		
	}
	public static double getValorHoras() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Indique o valor das Horas: ");
		double vh = sc.nextDouble();
		//valor hora
		return vh;		
	}
	public static double getPerDesc() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Indique o percentual de desconto: ");
		double pd = sc.nextDouble();
		//percentual de desconto
		return pd;		
	}
	public static double salBruto(double ht,double vh) {
		double salBruto = ht*vh;
		return salBruto;		
	}
	public static double totalDesc(double pd,double salBruto) {
		//total desconto
		double td = (pd/100)*salBruto;
		return td;
	}
	public static double salLiquido(double salBruto,double totalDesc) {
		//salario liquido
		double sl = salBruto - totalDesc;
		return sl;
	}
	

}
