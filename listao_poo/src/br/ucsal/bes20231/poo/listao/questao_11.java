package br.ucsal.bes20231.poo.listao;
import java.util.Scanner;

public class questao_11 {

	public static void main(String[] args) {
		
		printUtil.imprimir(volume(getRaio(),getAltura()));
		
	}
	public static double getRaio() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Indique o valor do Raio");
		double raio = sc.nextDouble();
		return raio;	
	}
	public static double getAltura() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Indique o valor da altura");
		double raio = sc.nextDouble();
		return raio;	
	}
	
	public static double volume(double raio,double altura) {
		double pi = 3.14159;
		double volume = pi*Math.pow(raio, 2)*altura;
		return volume;
	}

}
