package br.ucsal.bes20231.poo.listao;
import java.util.Scanner;

public class questao_13 {

	public static void main(String[] args) {
		trocarValores(getValorA(), getValorB());
	}
	
	public static double getValorA() {
		Scanner sc = new Scanner (System.in);	
		System.out.println("Insira o valor A");
		double valorA = sc.nextDouble();
		return valorA;		
	}
	public static double getValorB() {
		Scanner sc = new Scanner (System.in);	
		System.out.println("Insira o valor B");
		double valorB = sc.nextDouble();
		return valorB;		
	}
	public static void trocarValores(double valorA,double valorB) {
		double valor1 = valorA;
		double valor2=valorB;	
		valorB=valor1;
		valorA=valor2;
		System.out.println("Valor A trocado: "+ valorA);
		System.out.println("Valor B trocado: "+ valorB);
	}
	

}
